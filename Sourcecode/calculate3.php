<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

if(isset($_POST["money"]))
{
	$option = $_POST["option"];
	$money = $_POST["money"];
	$people = $_POST["people"];
	$discount_1 = $_POST["discount_1"];
	$discount_2 = $_POST["discount_2"];
	
	if($option == "0")
	{
		$total = ($money-($money*$discount_1/100))-$discount_2;
		$average = $total/$people;
		$average = number_format($average,2);
		echo("<h2> เฉลี่ยคนละ ".$average."  บาท </h2><br>");
	}
	else
	{
		$moneybalance = ($money-$discount_2);
		$total = $moneybalance-($moneybalance*$discount_1/100);
		$average = $total/$people;
		$average = number_format($average,2);	
		echo("<h2> เฉลี่ยคนละ ".$average."  บาท </h2><br>");
	}
}
?>