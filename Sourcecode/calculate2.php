<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");

$money = (int)$_GET["money"];
$deposit = (int)$_GET["deposit"];
$day = (int)$_GET["day"];
$cost = (int)$_GET["cost"];

$moneycal = (((int)$money) - (((int)$deposit) + ((int)$cost))) / ((int)$day);
$moneycal = number_format($moneycal,2);
		
if($moneycal >= 0)
	{
		$response = new stdClass();
		$response->moneycal = "เงินคงเหลือที่สามารถใช้ได้ $moneycal บาท/วัน";
		$return = json_encode($response);
	}
else
	{
		$response = new stdClass();
		$response->moneycal = "ไม่สามารถคำนวนได้เนื่องจากจำนวนเงินน้อยเกินไป";
		$return = json_encode($response);
	}
	

echo $return;
?>