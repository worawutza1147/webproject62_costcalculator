<?php
$result = ("<h3>----------------------------------<br> เฉลี่ยคนละ 0.00  บาท <br>----------------------------------</h3><br>");
if(isset($_POST["money"]))
{
	$option = $_POST["option"];
	$money = $_POST["money"];
	$people = $_POST["people"];
	$discount_1 = $_POST["discount_1"];
	$discount_2 = $_POST["discount_2"];
	
	if($option == "0")
	{
		$total = ($money-($money*$discount_1/100))-$discount_2;
		$average = $total/$people;
		$average = number_format($average,2);
		$result = ("<h3>----------------------------------<br> เฉลี่ยคนละ ".$average."  บาท <br>----------------------------------</h3><br>");
	}
	else
	{
		$moneybalance = ($money-$discount_2);
		$total = $moneybalance-($moneybalance*$discount_1/100);
		$average = $total/$people;
		$average = number_format($average,2);	
		$result = ("<h3>----------------------------------<br> เฉลี่ยคนละ ".$average."  บาท <br>----------------------------------</h3><br>");
	}
}
?>

<!DOCTYPE html>
<html>
<head>
    <title> ตัวช่วยค่าใช้จ่าย </title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<link rel="stylesheet" href="design2.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
	
</head>

<body>
	<nav class="navbar navbar-inverse navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.html" class="navbar-brand">CostCalculator</a>
				</div>
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
						<li><a href="http://localhost/sec.php">ตัวช่วยคิดเงิน</a></li>

					</ul>
				</div>
			</div>
	</nav>
	<br><br><br>

    <div class="container">
		<div class="jumbotron">
			<h1><center>ตัวช่วยคิดเงิน</center></h1>
			<h4><center>(สำหรับช่วยคิดเงินเฉลี่ยเป็นรายคน)</center></h4>
		</div>

		<div class ="row">
			<div class="col-sm-6">
				<h2>คำนวณเงิน</h2><br>
				
				<form action="sec.php" method="post">
				
				<label>ใส่จำนวนเงินทั้งหมดที่นี่</label><br>
				<input type="text" name="money" placeholder="ใส่จำนวนเงินของคุณ" > บาท <br><br>
				<label>จำนวนคน</label><br>
				<input type="text" name="people" placeholder="จำนวนคน"> คน <br><br>
				<label>ส่วนลด(ถ้ามี)</label><br>
				<input type="text" name="discount_1" placeholder="ส่วนลดเป็นเปอร์เซนต์"> %  &nbsp; &emsp; <Input type="radio" name="option" value="0" checked> คำนวณโดย ลดเปอร์เซนต์ ก่อน<br><br>
				<input type="text" name="discount_2" placeholder="ส่วนลดเป็นบาท"> บาท   &emsp; 		<Input type="radio" name="option" value="1"> คำนวณโดย ลดบาท ก่อน<br><br>
				<input type= "submit" class="btn btn-large" value="คลิกเพื่อคำนวณ">
				
				</form>
				<?php echo $result; ?>
			</div>
		</div>
	</div>
</body>
</html>