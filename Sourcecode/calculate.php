<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
	
	date_default_timezone_set('Asia/Bangkok');
	$date = new DateTime();
	
	(string)$day = $date->format('d');
	(string)$month = $date->format('m');
	(string)$year = $date->format('Y');
	
	
	intval($day);
	intval($month);
	intval($year);
	
	
	$check = (($year % 4 == 0) && ($year % 100 != 0) || ($year % 400 ==0));
	switch ($month)
	{
		case 2: $days = ($check? 29:28);
		break;
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12: $days = 31;
		break;
		case 4:
		case 6:
		case 9:
		case 11: $days = 30;
		break;
		
	}
	
	$date2 = new DateTime();

$money = (int)$_GET["money"];

$balance = $money/($days-$day);

$balance = number_format($balance,2);

$response = new stdClass();
$response->moneycal = $balance;
$response->remains = $days-$day;

$response->dateday = $day;
$response->dateyear = $year+543;

switch ($month)
{
	case 1: $response->datemonth = "มกราคม";	break;
	case 2:	$response->datemonth = "กุมภาพันธ์";	break;
	case 3:	$response->datemonth = "มีนาคม";	break;
	case 4:	$response->datemonth = "เมษายน";	break;
	case 5:	$response->datemonth = "พฤษภาคม";	break;
	case 6:	$response->datemonth = "มิถุนายน";	break;
	case 7:	$response->datemonth = "กรกฎาคม";	break;
	case 8:	$response->datemonth = "สิงหาคม";	break;
	case 9:	$response->datemonth = "กันยายน";	break;
	case 10:	$response->datemonth = "ตุลาคม";	break;
	case 11:	$response->datemonth = "พฤศจิกายน";	break;
	case 12:	$response->datemonth = "ธันวาคม";	break;
}


$return = json_encode($response);

echo $return;
?>